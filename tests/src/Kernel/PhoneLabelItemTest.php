<?php

declare(strict_types=1);

namespace Drupal\Tests\phone_label\Kernel;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests creating/editing phone label items with the entity API.
 *
 * @group phone_label
 */
class PhoneLabelItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['phone_label', 'telephone'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'entity_test',
      'type' => 'phone_label',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'field_test',
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Tests using entity fields of the phone_label field type.
   */
  public function testTestItem(): void {
    // Verify entity creation.
    $entity = EntityTest::create();
    $value = '+0123456789';
    $title = 'Foo Bar';
    $entity->field_test->value = $value;
    $entity->field_test->title = $title;
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $entity = EntityTest::load($id);
    $this->assertTrue($entity->field_test instanceof FieldItemListInterface, 'Field implements interface.');
    $this->assertTrue($entity->field_test[0] instanceof FieldItemInterface, 'Field item implements interface.');
    $this->assertEquals($value, $entity->field_test->value);
    $this->assertEquals($title, $entity->field_test->title);
    $this->assertEquals($value, $entity->field_test[0]->value);
    $this->assertEquals($title, $entity->field_test[0]->title);

    // Verify changing the field value.
    $new_value = '+41' . rand(1000000, 9999999);
    $new_title = 'Bar Foo';
    $entity->field_test->value = $new_value;
    $entity->field_test->title = $new_title;
    $this->assertEquals($new_value, $entity->field_test->value);
    $this->assertEquals($new_title, $entity->field_test->title);

    // Read changed entity and assert changed values.
    $entity->save();
    $entity = EntityTest::load($id);
    $this->assertEquals($new_value, $entity->field_test->value);
    $this->assertEquals($new_title, $entity->field_test->title);

    // Test sample item generation.
    $entity = EntityTest::create();
    $entity->field_test->generateSampleItems();
    $this->entityValidateAndSave($entity);
  }

}
