<?php

declare(strict_types=1);

namespace Drupal\phone_label\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\telephone\Plugin\Field\FieldType\TelephoneItem;

/**
 * Adds optional label settings to cores "telephone" field.
 *
 * @FieldType(
 *   id = "phone_label",
 *   label = @Translation("Labelled telephone number"),
 *   description = @Translation("This field stores a telephone number in the database."),
 *   category = @Translation("Number"),
 *   default_widget = "phone_label_default",
 *   default_formatter = "basic_string"
 * )
 */
class PhoneLabelItem extends TelephoneItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'title' => [
          'description' => 'The label text.',
          'type' => 'varchar',
          'length' => 256,
        ],
        'value' => [
          'type' => 'varchar',
          'length' => 256,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Phone label'));

    return $properties + TelephoneItem::propertyDefinitions($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element['title'] = [
      '#type' => 'radios',
      '#title' => t('Allow label override'),
      '#description' => t('Allow the phone link label to be customised. Label defaults to the entered phone number if disabled or optional is selected.'),
      '#default_value' => $this->getSetting('title'),
      '#options' => [
        DRUPAL_DISABLED => t('Disabled'),
        DRUPAL_OPTIONAL => t('Optional'),
        DRUPAL_REQUIRED => t('Required'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'title' => DRUPAL_DISABLED,
    ];
  }

}
