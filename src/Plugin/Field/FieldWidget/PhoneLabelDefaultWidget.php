<?php

declare(strict_types=1);

namespace Drupal\phone_label\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telephone\Plugin\Field\FieldWidget\TelephoneDefaultWidget;

/**
 * Plugin implementation of the 'phone_label_default' widget.
 *
 * @FieldWidget(
 *   id = "phone_label_default",
 *   label = @Translation("Labelled phone number"),
 *   field_types = {
 *     "phone_label"
 *   }
 * )
 */
class PhoneLabelDefaultWidget extends TelephoneDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder_title' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['placeholder_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for label text'),
      '#default_value' => $this->getSetting('placeholder_title'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
      '#states' => [
        'invisible' => [
          ':input[name="instance[settings][title]"]' => ['value' => DRUPAL_DISABLED],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $show_title_field = $this->getFieldSetting('title') !== DRUPAL_DISABLED;

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#title_display' => 'before',
      '#default_value' => $items[$delta]->title ?? NULL,
      '#placeholder' => $this->getSetting('placeholder_title'),
      '#maxlength' => 255,
      '#access' => $show_title_field,
      '#required' => $this->getFieldSetting('title') === DRUPAL_REQUIRED && $element['#required'],
      '#weight' => 2,
    ];

    // If this is a multi-field, add a label do the value field.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() !== 1) {
      $element['value']['#title'] = $this->t('Telephone number');
      $element['value']['#title_display'] = 'before';
    }

    if ($show_title_field) {
      $element += [
        '#type' => 'fieldset',
      ];
    }

    return $element;
  }

}
