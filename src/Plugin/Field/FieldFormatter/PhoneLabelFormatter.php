<?php

declare(strict_types=1);

namespace Drupal\phone_label\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'phone_label' formatter.
 *
 * @FieldFormatter(
 *   id = "phone_label",
 *   label = @Translation("Telephone link with label"),
 *   field_types = {
 *     "phone_label"
 *   }
 * )
 */
class PhoneLabelFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $phone_number = preg_replace('/\s+/', '', $item->value);
      if (strlen($phone_number) <= 5) {
        $phone_number = substr_replace($phone_number, '-', 1, 0);
      }

      // Render each element as link.
      $element[$delta] = [
        '#type' => 'link',
        // Use custom title if available, otherwise use the telephone number
        // itself as title.
        '#title' => $item->title ? $item->title : $item->value,
        // Prepend 'tel:' to the telephone number.
        '#url' => Url::fromUri('tel:' . rawurlencode($phone_number)),
        '#options' => ['external' => TRUE],
      ];

      if (!empty($item->_attributes)) {
        $element[$delta]['#options'] += ['attributes' => []];
        $element[$delta]['#options']['attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    return $element;
  }

}
